node-gauge (4.0.4-1+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 07 Apr 2023 17:59:39 +0000

node-gauge (4.0.4-1) unstable; urgency=medium

  * Team upload
  * Declare compliance with policy 4.6.1
  * New upstream version 4.0.4

 -- Yadd <yadd@debian.org>  Thu, 19 May 2022 10:54:59 +0200

node-gauge (4.0.3-1) unstable; urgency=medium

  * Team upload
  * New upstream version 4.0.3
  * Fix test for tap >= 15 (--no-cov)

 -- Yadd <yadd@debian.org>  Sat, 12 Mar 2022 09:32:46 +0100

node-gauge (4.0.2-1) unstable; urgency=medium

  * Team upload
  * Set upstream metadata fields: Security-Contact.
  * New upstream version 4.0.2

 -- Yadd <yadd@debian.org>  Thu, 24 Feb 2022 08:55:31 +0100

node-gauge (4.0.0-1) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.
  * Remove constraints unnecessary since buster:
    + node-gauge: Drop versioned constraint on node-aproba,
      node-console-control-strings, node-has-unicode, node-object-assign,
      node-signal-exit, node-string-width, node-strip-ansi and node-wide-align
      in Depends.

  [ Yadd ]
  * Bump debhelper from old 12 to 13.
  * Declare compliance with policy 4.6.0
  * Add "Rules-Requires-Root: no"
  * Change section to javascript
  * Add debian/gbp.conf
  * Modernize debian/watch
  * Fix filenamemangle
  * Fix GitHub tags regex
  * Drop dependency to nodejs
  * Use dh-sequence-nodejs auto install
  * Update upstream source to https://github/npm/gauge
  * New upstream version 4.0.0
  * Update dependencies
  * Drop patch
  * Enable upstream test (tap)

 -- Yadd <yadd@debian.org>  Sat, 20 Nov 2021 08:58:22 +0100

node-gauge (2.7.4-1.1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 18:38:10 +0000

node-gauge (2.7.4-1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sun, 21 Feb 2021 10:41:26 +0000

node-gauge (2.7.4-1) unstable; urgency=low

  * Initial release (Closes: #862255)

 -- Pirate Praveen <praveen@debian.org>  Wed, 10 May 2017 19:09:51 +0530
